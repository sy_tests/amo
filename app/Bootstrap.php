<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

spl_autoload_register(function($name){
    require_once __DIR__ . "/" . str_replace('App', '', str_replace("\\","/",$name)).".php";
});
