<?php
/**
 * User: sergei
 * Date: 06.03.19
 */

namespace App;


class Config
{

    /**
     * @var array
     */
    private static $_instance = null;

    /**
     * @var array
     */
    private $config;

    /**
     * Config constructor.
     */
    private function __construct()
    {
        $this->config = require_once('../config/app.php');
    }

    /**
     * Returns the instance.
     *
     * @static
     * @return Config
     */
    public static function getInstance()
    {
        if (self::$_instance == null) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    /**
     * Get a config item.
     *
     * @param $path
     *
     * @return mixed
     */
    public function get($path)
    {
        $res = null;
        if (isset($path)) {
            $path   = explode('.', $path);
            $config = $this->config;

            $getValue = function($keys, $config) use (&$getValue) {
                $key = array_shift($keys);
                if(isset($config[$key])) {
                    if($keys) {
                        $res = $getValue($keys, $config[$key]);
                    } else {
                        $res = $config[$key];
                    }
                } else {
                    $res = null;
                }

                return $res;
            };

            $res = $getValue($path, $config);
        }
        return $res;
    }

    private function __clone() {}
    private function __wakeup() {}

    public function __destruct()
    {
        self::$_instance = null;
    }
}