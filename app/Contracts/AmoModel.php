<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Contracts;


interface AmoModel
{
    /**
     * @return array
     */
    public function getPrepareArray();
}