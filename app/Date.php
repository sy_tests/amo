<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App;


class Date extends \DateTime
{
    /**
     * @param string $timezone
     * @return int
     */
    public static function now($timezone = 'UTC')
    {
        $time = new self('now', new \DateTimeZone($timezone));
        return $time->getTimestamp();
    }
}