<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Forms;

use App\Methods\Account;
use App\Methods\Authorize;

class Base
{
    /**
     * @var array
     */
    public $user = [];
    /**
     * @var array
     */
    public $account_data = [];

    /**
     * Base constructor.
     */
    public function __construct()
    {
        $this->authorize();
//        $this->initAccountData();
    }

    /**
     * Authorization
     */
    public function authorize()
    {
        $authRequest = new Authorize;
        $authRequest->exec();

        $this->user = $authRequest->getResponse();
    }

//    /**
//     * Init account data
//     */
//    public function initAccountData()
//    {
//        $accountRequest = new Account('custom_fields');
//        $accountRequest->exec();
//
//        $this->account_data = $accountRequest->getResponse();
//    }
}