<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Forms;

use App\Date;
use App\Methods\Account;
use App\Methods\Contact\AddContact;
use App\Methods\Lead\AddLead;
use App\Methods\Contact\FindContact;
use App\Methods\Lead\FindLead;
use App\Methods\Task\AddTask;
use App\Models\Contact;
use App\Models\Lead;
use App\Models\Task;
use App\Request;

class MainForm extends Base
{
    /**
     * Submit new contact
     * @param Request $request
     */
    public function submit(Request $request)
    {
        $is_contact_exists = false;
        $post = $request->post();
        if($post['phone'] && $post['email']) {

            $contact = (new FindContact(['query' => $post['phone']]))->exec()->getResponse();

            if(!$contact) {
                $contact = (new FindContact(['query' => $post['email']]))->exec()->getResponse();
            }

            if(!$contact) {
                $contact = $this->addContact($post);
                $contact = (new FindContact(['id' => $contact['_embedded']['items'][0]['id']]))->exec()->getResponse();
                $id = $contact['id'];
            } else {
                $is_contact_exists = true;
            }

            if($contact && ($contact = current($contact['_embedded']['items']))) {
                if($is_contact_exists) {
                    $responsible_user_id = $contact['responsible_user_id'];
                } else {
                    $responsible_user_id = $this->getOptimalResponsibleId();
                }

                $this->addLead(['responsible_user_id' => $responsible_user_id, 'contacts_id' => $contact['id']]);
                $this->addTask([
                    'responsible_user_id' => $responsible_user_id,
                    'task_type' => 1,
                    'complete_till_at' => (new Date('now'))->modify('+1 day')->getTimestamp()
                ]);
            }
        }
    }

    /**
     * @param array $post
     * @return array
     */
    protected function addContact(array $post)
    {
        $contact = new Contact;
        $contact->name = $post['name'];

        $contact->setCustomField(['code' => 'PHONE'], ['value' => $post['phone'], 'enum' => 'WORK']);
        $contact->setCustomField(['code' => 'EMAIL'], ['value' => $post['email'], 'enum' => 'WORK']);

        return (new AddContact($contact->getPrepareArray()))
            ->exec()
            ->getResponse();
    }

    /**
     * @param array $data
     * @return array
     */
    protected function addLead(array $data)
    {
        $lead = new Lead();
        $lead->name = 'Заявка с сайта';
        $lead->responsible_user_id = $data['responsible_user_id'];
        $lead->contacts_id = $data['contacts_id'];

        return (new AddLead($lead->getPrepareArray()))
            ->exec()
            ->getResponse();
    }

    /**
     * @param array $data
     * @return array
     */
    protected function addTask(array $data)
    {
        $task = new Task();
        $task->task_type = $data['task_type'];
        $task->responsible_user_id = $data['responsible_user_id'];
        $task->complete_till_at = $data['complete_till_at'];

        return (new AddTask($task->getPrepareArray()))
            ->exec()
            ->getResponse();
    }

    /**
     * @return int|null
     */
    protected function getOptimalResponsibleId()
    {
        $responsibleByContact = $leadUsers = [];
        if($users = (new Account('users'))->exec()->getResponse()) {
            foreach($users['_embedded']['users'] as $user) {
                if(!$user['is_admin'] && $user['is_active']) {
                    $leadUsers[$user['id']] = 0;
                }
            }
        }

        $prevDay = (new Date('now'))->modify('-1 day')->getTimestamp();
        $leads = (new FindLead(['filter' => ['date_create' => ['from' => $prevDay, 'to' => Date::now()]]]))
            ->exec()
            ->getResponse();

        if(!$leads) {
            return current(array_keys($leadUsers));
        } else {
            foreach($leads['_embedded']['items'] as $lead) {
                if(isset($leadUsers[$lead['responsible_user_id']])) {
                    if (!isset($responsibleByContact[$lead['responsible_user_id']])) {
                        $leadUsers[$lead['responsible_user_id']] = 1;
                        $responsibleByContact[$lead['responsible_user_id']] = [];
                    } elseif (!isset($responsibleByContact[$lead['responsible_user_id']][$lead['main_contact']['id']])) {
                        $responsibleByContact[$lead['responsible_user_id']][$lead['main_contact']['id']] = true;
                        $leadUsers[$lead['responsible_user_id']]++;
                    }
                }
            }
        }

        return $leadUsers ? current(array_keys($leadUsers, min($leadUsers))) : null;
    }
}