<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Methods;

class Account extends Base
{
    /**
     * @var string
     */
    public $urn = '/api/v2/account';

    /**
     * Authorize constructor.
     */
    public function __construct($urn_with)
    {
        $this->urn .= '?with=' . $urn_with;
        parent::__construct([]);
    }
}