<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Methods;


use App\Config;

class Authorize extends Base
{
    /**
     * @var string
     */
    public $urn = '/private/api/auth.php?type=json';

    /**
     * Authorize constructor.
     */
    public function __construct()
    {
        parent::__construct([
            'USER_LOGIN' => Config::getInstance()->get('api.login'),
            'USER_HASH' => Config::getInstance()->get('api.key')
        ]);
    }
}