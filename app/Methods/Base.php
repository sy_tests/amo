<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Methods;

use App\Config;
use App\CurlRequest;

class Base
{
    /**
     * @var CurlRequest|null
     */
    protected $curl_handle = null;
    /**
     * @var string
     */
    protected $urn = '';
    /**
     * @var array
     */
    protected $data = [];
    /**
     * @var array
     */
    protected $errors = [
        301=>'Moved permanently',
        400=>'Bad request',
        401=>'Unauthorized',
        403=>'Forbidden',
        404=>'Not found',
        500=>'Internal server error',
        502=>'Bad gateway',
        503=>'Service unavailable'
    ];
    /**
     * @var array
     */
    protected $response = [];

    /**
     * Base constructor.
     * @param string $data
     */
    public function __construct($data = '')
    {
        $this->initHeader($data);
    }

    /**
     * @param $data
     */
    protected function initHeader($data)
    {
        $this->curl_handle = new CurlRequest;
        $this->curl_handle->setOption(CURLOPT_RETURNTRANSFER, true);
        $this->curl_handle->setOption(CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        $this->curl_handle->setOption(CURLOPT_URL, Config::getInstance()->get('api.url') . $this->urn);
        $this->curl_handle->setOption(CURLOPT_HEADER,false);
        $this->curl_handle->setOption(CURLOPT_COOKIEFILE, '/tmp/amo_cookie.txt');
        $this->curl_handle->setOption(CURLOPT_COOKIEJAR, '/tmp/amo_cookie.txt');
        $this->curl_handle->setOption(CURLOPT_SSL_VERIFYPEER,0);
        $this->curl_handle->setOption(CURLOPT_SSL_VERIFYHOST,0);

        if($data) {
            $this->setData($data);
        }
    }

    /**
     * @return $this
     */
    public function exec()
    {
        $this->response = $this->curl_handle->execute(false);
        $code = $this->curl_handle->getInfo(CURLINFO_HTTP_CODE);

        try
        {
            if($code != 200 && $code != 204) {
                throw new \Exception(isset($this->errors[$code]) ? $this->errors[$code] : 'Undescribed error',$code);
            }
        }
        catch(\Exception $E)
        {
            die('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());
        }

        return $this;
    }

    /**
     * @param $data
     */
    public function setData($data)
    {
        $this->curl_handle->setOption(CURLOPT_POSTFIELDS, json_encode($data));
        $this->curl_handle->setOption(CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        $this->curl_handle->setOption(CURLOPT_CUSTOMREQUEST, 'POST');
    }

    /**
     * @return array`
     */
    public function getResponse()
    {
        return json_decode($this->response,true);
    }
}