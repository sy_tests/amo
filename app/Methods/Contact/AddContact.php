<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Methods\Contact;

use App\Config;
use App\Methods\Base;

class AddContact extends Base
{
    /**
     * @var string
     */
    public $urn = '/api/v2/contacts';

    /**
     * Authorize constructor.
     */
    public function __construct($data)
    {
        parent::__construct(['add' => [$data]]);
    }
}