<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Methods\Contact;

use App\Config;
use App\Methods\Base;

class FindContact extends Base
{
    /**
     * @var string
     */
    public $urn = '/api/v2/contacts/';

    /**
     * FindContact constructor.
     * @param array $query
     */
    public function __construct(array $query)
    {
        $this->urn .= '?' . http_build_query($query);
        parent::__construct([]);
    }
}