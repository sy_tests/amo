<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Methods\Lead;

use App\Config;
use App\Methods\Base;

class AddLead extends Base
{
    /**
     * @var string
     */
    public $urn = '/api/v2/leads';

    /**
     * Authorize constructor.
     */
    public function __construct($data)
    {
        parent::__construct(['add' => [$data]]);
    }
}