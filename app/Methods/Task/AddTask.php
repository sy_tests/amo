<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Methods\Task;

use \App\Methods\Base;

class AddTask extends Base
{
    /**
     * @var string
     */
    public $urn = '/api/v2/tasks';

    /**
     * Authorize constructor.
     */
    public function __construct($data)
    {
        parent::__construct(['add' => [$data]]);
    }
}