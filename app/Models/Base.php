<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Models;

use App\Contracts\AmoModel;

class Base implements AmoModel
{
    /**
     * Listed custom fields that belong to this model
     * @var array
     */
    protected $custom_fields = [];

    /**
     * @return array
     */
    public function getPrepareArray()
    {
        $res = [];
        $reflect = new \ReflectionClass($this);
        $props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC);

        if($props) {
            foreach($props as $prop) {
                $res[$prop->name] = $this->{$prop->name};
            }
        }

        if($this->custom_fields) {
            $res['custom_fields'] = $this->custom_fields;
        }

        return $res;
    }

    /**
     * @param $field
     * @param $value
     */
    public function setCustomField($field, $value)
    {
        $field = is_array($field) ? $field : [$field];
        $field['values'][] = (is_array($value) ? $value : ['value' => $value]);
        $this->custom_fields[] = $field;
    }
}