<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Models;


class Lead extends Base
{
    /**
     * @var
     */
    public $name;
    /**
     * @var
     */
    public $responsible_user_id;
    /**
     * @var
     */
    public $contacts_id;
}