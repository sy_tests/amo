<?php
/**
 * User: Sergei
 * Date: 06.03.19
 */

namespace App\Models;


class Task extends Base
{
    /**
     * @var
     */
    public $task_type;
    /**
     * @var
     */
    public $responsible_user_id;
    /**
     * @var
     */
    public $complete_till_at;
}