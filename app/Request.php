<?php
/**
 * Created by PhpStorm.
 * User: sergei
 * Date: 06.03.19
 */

namespace App;

use App\Models\User;

class Request
{
    /**
     * @var string
     */
    public $urn;
    /**
     * @var array | null
     */
    protected $query = null;
    /**
     * @var array | null
     */
    protected $post = null;
    /**
     * @var array | null
     */
    protected $session = null;
    /**
     * @var null|array
     */
    protected $user = null;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->urn = preg_replace('/\?.*/','',trim($_SERVER['REQUEST_URI'],'/'));
    }

    /**
     * @return array
     */
    public function query()
    {
        if(is_null($this->query)) {
            $this->query = $_GET;
        }

        return $this->query;
    }

    /**
     * @return array
     */
    public function post()
    {
        if(is_null($this->post)) {
            $this->post = $_POST;
        }

        return $this->post;
    }

    /**
     * @return array|null
     */
    public function session()
    {
        if(is_null($this->session)) {
            $this->session = $_SESSION;
        }

        return $this->session;
    }

    /**
     * @return array|mixed|null
     */
    public function user()
    {
        if(is_null($this->user) && $this->session()) {
            $user_id = $this->session()['uid'];
            $this->user = (new User)->getById($user_id);
        }

        return $this->user;
    }
}