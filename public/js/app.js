function validate(form_id) {
    var isValid = true;
    var emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var phoneReg = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    var email = document.forms[form_id].elements['email'].value;
    var phone = document.forms[form_id].elements['phone'].value;
    var emailError = document.getElementById('emailError');
    var phoneError = document.getElementById('phoneError');

    if(emailReg.test(email) == false) {
        emailError.style.display = 'block';
        emailError.innerHTML = 'Введите корректный e-mail';
        isValid = false;
    } else {
        emailError.style.display = 'none';
    }

    if(phoneReg.test(phone) == false) {
        phoneError.style.display = 'block';
        phoneError.innerHTML = 'Введите корректный телефон';
        isValid = false;
    } else {
        phoneError.style.display = 'none';
    }

    if(!isValid) {
        return false;
    }
}