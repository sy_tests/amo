<?php

require_once '../app/Bootstrap.php';

(new App\Forms\MainForm)->submit(
    (new \App\Request())
);

header('Location: /index.html', '302');